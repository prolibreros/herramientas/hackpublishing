require 'hackpublishing/extensions'
require 'hackpublishing/common'
require 'colorize'

module Hackpublishing
  class MD2TTY

  # Converts MD file to TTY output
    def initialize file_path
      $log.save(self, __method__)

      # Replaces MD syntax
      @txt = File.read(file_path)
        .split(/\n{2,}/)
        .map{|b| b !~ /^```/ ? b =~ /^\(\s+/ ? '' : b.to_tty : b.to_tty_code}
        .join("\n\n")
        .gsub(/\n{2,}/, "\n\n")
        .gsub(/\n+$/, "\n")
    end

    # Displays TTY output
    def display
      $log.save(self, __method__)
      puts '', @txt
    end
  end
end
