require 'hackpublishing/log'
require 'hackpublishing/md2tty'
require 'i18n'
require 'yaml'

# Global variables
$cmds = ['credits', 'go', 'help', 'init', 'install', 'log', 'toc', 'verify']
$root = File.expand_path(__dir__ + '/../../src') + '/'
$conf = '.hackpublishing.conf'
$logf = '.hackpublishing.log'
$log  = Hackpublishing::Log.new

# Prints MD texts
def prt file, warn = true
  $log.save(self, __method__.to_s + '[' + warn.to_s + ']: ' + file)

  file  = File.basename(file, '.*') + '.md'
  path  = get_root_txt + file

  if warn
    puts I18n.t(:warn)
  end

  Hackpublishing::MD2TTY.new(path).display
end

# Prints help
def prt_help with_error = true
  if with_error
    $log.save(self, 'I18n.t(:errors)[:par]')
    puts I18n.t(:errors)[:par].colorize(:red)
  end
  puts '', I18n.t(:help)[0].colorize(:green)
  puts '  ' + I18n.t(:help)[1]
end

# Prints error when outside
def prt_error_inside
  $log.save(self, 'I18n.t(:errors)[:inside]')
  puts I18n.t(:errors)[:inside].colorize(:red)
  abort
end

# Prints success
def prt_success type, position, id
  $log.save(self, 'I18n.t(:success)::' + type + '::' + position)
  puts '', I18n.t(:success)[type.to_sym].colorize(:blue).blink
  if id.to_i <= 1
    puts '', I18n.t(position.to_sym)[0].colorize(:green)
    puts '  ' + I18n.t(position.to_sym)[1]
  end
end

# Checks if is inside a study room
def inside? conf = 0

  # 0 == boolean
  # 1 == config file path
  # 2 == config file content
  # 3 == prt_error

  def dig path, conf
    begin
      if Dir.children(path).include?($conf)
        if conf == 1
          path + '/' + $conf
        elsif conf == 2
          YAML.load_file(path + '/' + $conf)
        else
          true
        end
      else
        dig(path + '/..', conf)
      end
    rescue
      if conf == 3 then prt_error_inside else false end
    end
  end

  dig(Dir.pwd, conf)
end

# Gets configuration
def get_config path = false
  if path
    return inside?(1)
  else
    return inside?(2)
  end
end

# Gets available langs
def get_langs
  return Dir.children($root + 'txt/')
end

# Gets lesson by id
def get_lesson id
  begin
    return get_lessons.select {|l| l[:id] == id}.first
  rescue
    return nil
  end
end

# Gets all lessons
def get_lessons
  root  = get_root_txt
  paths = Dir.children(root).reject{|e| e !~ /^lesson_/}.sort
  names = []

  paths.each do |p|
    path1    = root + p 
    path2    = root.gsub('/txt/', '/yml/') +
               p.gsub(/^lesson(.*?)\.md/, 'solution' + '\1' + '.yml') 
    name     = File.read(root + p).gsub(/(.|\n)*?^#+\s*(.+?)$/, '\2')
               .split(/\n+/)[0].strip
    id       = name.split(' ')[0][0..-2]
    solution = File.exists?(path2) ? path2 : nil

    names.push({
      :id => id,
      :name => name,
      :path => path1,
      :solution => solution
    })
  end

  return names
end

# Gets just all lessons ids
def get_lessons_ids
  return get_lessons.map{|l| l.select{|k, v| [:id].include?(k)}[:id]}
end

# Gets log file path
def get_log_path
  config_path = get_config(1).to_s
  return File.dirname(config_path) + '/' + $logf
end

# Gets solutions from yml file
def get_solution lesson
  file_path = lesson[:solution]
  return file_path ? YAML.load_file(file_path) : nil
end

# Gets root of txt files
def get_root_txt
  if inside?
    return $root + 'txt/' + get_config['lang'] + '/'
  else
    return $root + 'txt/en/'
  end
end

# Loads stdouts
I18n.load_path << Dir[$root + "../config/locales/*.yml"]
I18n.default_locale = inside? ? get_config['lang'].to_sym : :en
