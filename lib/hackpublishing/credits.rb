require 'hackpublishing/common'

module Hackpublishing
  class Credits

    # Displays credits
    def initialize
      $log.save(self, __method__)
      prt('credits')
    end
  end
end
