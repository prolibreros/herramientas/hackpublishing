require 'hackpublishing/common'

module Hackpublishing
  class Help

    # Displays help
    def initialize
      $log.save(self, __method__)
      prt('splash-screen')
      prt('help', false)
    end
  end
end
