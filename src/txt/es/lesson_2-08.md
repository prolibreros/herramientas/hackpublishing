# 2.8. Mis comandos

Si ejecutaste `$ … help` viste que hay más comandos, como los que te permiten
ver los créditos de este manual (`$ … credits`).

En caso de perderte, recuerda que:

- `$ … go` siempre te lleva a la siguiente lección incompleta.
- `$ … toc` te muestra el mapa de las lecciones y secciones de tu sala.
- `$ … help` te despliega un recordatorio de cómo usar `hackpublishing`.
