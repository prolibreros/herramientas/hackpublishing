# 1.3. ¿Por qué «tecnologías editoriales libres»?

Los procesos y técnicas presentes en este manual son aplicables en una variedad
de _software_ y _hardware_. Pero los años y las frustraciones nos han enseñado
que su resilencia aumenta con tecnologías libres. La definición (dogmática) de
«libertad» tecnológica dicta que el usuario ha de poder

0. **usarla** para cualquier propósito,
1. **estudiarla** y **adaptarla** a sus necesidades,
2. **difundirla** en beneficio de más usuarios y
3. **mejorarla** en pos de su comunidad.

( La tecnología no se reduce al _software_ y _hardware_. Más bien me refiero a
( las «tecnologías de la información y la comunicación», aunque este traslado
( semántico siga sin dar claridad al término.

Una tecnología es libre si no restringe estas «cuatro libertades del _software_
libre». Por ello, aquí usaremos formatos y programas que permiten más
interoperatibilidad tecnológica en pos de la conservación de la información.

> **¿Sabías que…?** Las cuatro libertades del _software_ se presentaron por
> primera vez en [_El manifiesto de GNU_](http://alturl.com/fe6vu). Este fue una
> piedra angular para el movimiento del _software_ libre, la iniciativa del
> código abierto y los movimientos de la cultura libre y del acceso abierto.
