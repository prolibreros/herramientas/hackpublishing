```
                  .-----.░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
       .--.       |~~~~~|░░░░░░░░█████░░░░░.--> $ echo "¿Cómo hacer un libro?"
       |__|       |~~~~~|░░░░░░░░█ H █░░░.'░░░░ $ vim libro.md
.--.---|--|_      |     |--.░░░░░█ A █<-'░░░░░░ $ pandoc libro.md -o libro.pdf
|  |===|  |'\     | FI  |--|░░░░░█ C █░░░░░░░░░ $ pecas libro.md
|%%| A |  |.'\    | LO  |  |<--->█ K █░░░░░░░░░ $ ls
|%%| R |  |\.'\   | SO  |  |░░░░░█ S █░░░░░░░░░ libro.epub  libro.mobi
|  | T |  | \  \  | FÍA |  |░░░░░█████░░░░░░░░░ libro.html  libro.pdf
|  | E |__|  \.'\ |     |__|░░░░░░░'.░░░░░░░░░░ $ git add .
|  |===|--|   \.'\|~~~~~|--|░░░░░░░░░|░░░░░░░░░ $ git commit -m "¡A compartir!"
^--^---'--^    `-'^-----'--'░░░░░░░░░V░░░░░░░░░ $ git push origin master
```

# 0.3. Y juntos idearon otro mundo para hacer libros…

( Para Programando LIBREros fueron las artes y la filosofía lo que nos trajo,
( primero, al mundo de los libros y, después, al mundo de la técnica para hacer
( libros y, por último, al mundo del desarrollo tecnológico de herramientas
( editoriales.

( Pero los caminos para la «hackedición» son muy diversos. En nuestra comunidad
( conocemos personas que llegaron por la necesidad de utilizar otras
( herramientas, por el interés en otras áreas, como la computación, la
( antropología o la sociología, o hasta por el mero afán de compartir
( información considerada relevante.

( En este manual solo probarás algunas vías para poder hacer, deshacer o
( rehacer libros. El uso de la herramienta o el conocimiento de los procesos no
( es más relevante que la comprensión de que los formatos que utilizas y los
( recursos de cómputo con los que cuentas no limitan tus posibilidades de
( PRDC cultural, aunque quizá sí limite tu visibilidad en el lado A de internet.
