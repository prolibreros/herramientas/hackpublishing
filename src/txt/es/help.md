Uso:
  hackpublishing [ARGUMENTOS]

  Ir a una lección:
    hackpublishing go
      => Va y muestra la siguiente lección disponible.
    hackpublishing go NÚMERO_LECCIÓN
      => Va y muestra la lección solicitada.

  Ver la tabla de contenidos:
    hackpublishing toc
      => Muestra la tabla de contenidos.

  Verificar lecciones:
    hackpublishing verify
      => Verifica la lección actual.

  Iniciar una sala de estudio:
    hackpublishing init
      => Inicia una nueva sala de estudio en modo interactivo.
    hackpublishing init IDIOMA_DISPONIBLE
      => Inicia una nueva sala de estudio en modo silencioso.

  Gestionar el registro:
    hackpublishing log
      => Muestra el registro de la sala de estudio.
    hackpublishing log clean
      => Limpia el registro de la sala de estudio.

  Ver créditos:
    hackpublishing credits
      => Muestra los créditos y licencias de esta gema.

  Ver ayuda:
    hackpublishing help
      => Muestra esta ayuda.

¿Necesitas más soporte? Envíanos un correo a hi@programando.li :)
