# 1.1. ¿Qué es «hackedición»?

«Hola, mundo» es lo primero que hacemos en un manual de computación.
_Hackedición. Manual introductorio a tecnologías editoriales libres_ hace honor
a esa tradición al mismo tiempo que propone otra manera de usar un manual.

Hackedición no tiene páginas ni es un archivo abierto por tu lector. Hackedición
es _software_ pedagógico hecho con Ruby y distribuido en una gema ejecutable.

Hackedición es un programa para la [terminal](http://alturl.com/smnzo) que se
ejecuta con `$ hackpublishing`. Pero _la hackedición_ es más que eso, es una
manera de hacer libros y de ser parte de la cultura escrita. ¿Has malabareado
con la edición y el acceso a tus publicaciones para su enmiendo, transformación,
mejora o difusión? Eso ya es hackedición.

( El nombre del programa está en inglés por la «internacionalización», donde el
( inglés según es la nueva lengua franca del «mundo conocido».

El término «hackedición» fue acuñado por @hacklib y no es de fácil definición.
Nunca ha quedado claro qué es editar. Tampoco existe consenso sobre qué es
«_hacking_». Aquí constreñimos la edición a los libros; ten presente esta
arbitrariedad. Sobre los «_hack_», @Aradnix da una aproximación oportuna:

> _Hack_ es un verbo con muchos significados bastante variados entre sí. Pero la
> exploración lúdica, programar algo por gusto o poder encontrar una solución
> creativa a un problema o a una limitación puede considerarse un _hack_. Tiene
> que ver más con la cultura _hacker_ que con el robo de información.

( Por el enfoque a la edición de libros opté por «_publishing_» en lugar de
( «_edit_». En inglés «_edit_» remite más a la edición en general. Mientras que
( _en el mundo en el que me desenvuelvo_, «edición» apela más a los libros. Bien
( podría ser _hackedit_ en lugar de _hackpublishing_: es otra arbitrariedad.

Con esto podemos decir que la «hackedición» es dar con soluciones creativas a
los problemas y limitaciones presentes en la producción de libros. ¿Cuáles son
estas dificultades? Poco a poco veremos que van muy de la mano con nuestra
pérdida de libertad como editores, pero también como lectores y escritores.

( En un principio tenía mis reservas con el neologismo «_hackedición_». No soy
( fan del cuño de palabras sino de su resignificación. No obstante, el término
( me parece muy atractivo para los lectores.

( Desde los noventa la edición ha sufrido un cambio muy importante cuando según
( el mundo del consumo triunfó sobre el socialista. En México esto tomó forma
( con el TLC y la cultura salinista: una producción cultural organizada a partir
( de subsidios gubernamentales en pos de su consumo masivo o su «vanguardia».

( Una consecuencia de este nuevo paradigma en la producción fue el aumento
( incesante en la publicación de ejemplares, pero sin un incremento equitativo
( en su venta o en el número de lectores. Las productos editoriales más exitosos
( en esta situación fueron los libros de texto o de autoayuda.

( Para quienes editamos humanidades, filosofía o literatura, hemos tenido la
( necesidad de buscar estrategias de _marketing_ para captar mercado en un mundo
( donde el lector ha dejado de acercarse a los soportes editoriales consagrados,
( aunque varios humanistas aún se aferren a su culto.

( Algunas estrategias son la reedición de _longsellers_, el fichaje de autores
( célebres y los títulos seductores. El uso de «hackedición» para el título de
( este manual no fue fortuito, sino por la necesidad de captar tu atención.
