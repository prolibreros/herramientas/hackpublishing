# 2.1. Mi directorio

Este manual se realiza en una sala de estudio cuyo lugar es un ==directorio==
(un tecnicismo para «carpeta») nombrado `study-room`. Desde ahí accedes a las
secciones de este manual. Una sección es un conjunto de lecciones que te enseñan
técnicas específicas y procesos para poder publicar de la manera hacker >:)

Si estás leyendo este mensaje quiere decir que has aprendido a ejecutar un
==comando== muy importante: para moverte a través de las lecciones y secciones
basta con que escribas `$ hackpublishing go`.
