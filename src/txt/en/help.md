Usage:
  hackpublishing [ARGUMENTS]

  Go to a lesson:
    hackpublishing go
      => Goes and displays the next available lesson.
    hackpublishing go LESSON_NUMBER
      => Goes and displays the required lesson.

  See table of contents:
    hackpublishing toc
      => Displays the table of contents.

  Verify lessons:
    hackpublishing verify
      => Verifies the current lesson.

  Set a study room:
    hackpublishing init
      => Initializes a new study room in interactive mode.
    hackpublishing init LANG_AVAILABLE
      => Initializes a new study room in quiet mode.

  Manage the log:
    hackpublishing log
      => Displays study room' log.
    hackpublishing log clean
      => Cleans study room' log.

  See credits:
    hackpublishing credits
      => Displays gem's credits and licenses.

  See help:
    hackpublishing help
      => Displays this help.

Do you need more support? Email us at hi@programando.li :)
