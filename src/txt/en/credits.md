```
   ____________________________________________________________________________
 /                                                                             \
|    _____________________________________________________________________     |
|   |                                                                     |    |
|   |  $ credits                                                          |    |
|   |                            Hackpublishing                           |    |
|   |         Introductory manual to free publishing technologies         |    |
|   |          www.hack.programando.li/breros | hi@programando.li         |    |
|   |                                                                     |    |
|   |  Product offered by                                                 |    |
|   |    Programando LIBREros <www.programando.li/breros>                 |    |
|   |  Project sponsored by                                               |    |
|   |    Mexican National Fund for Culture and Arts (FONCA)               |    |
|   |  Free, open and libre software powered by                           |    |
|   |    Perro Tuerto <www.perrotuerto.blog>                              |    |
|   |                                                                     |    |
|   |  Testers: Mel, Antílope                                             |    |
|   |                                                                     |    |
|   |  Write new lessons or edit and translate entire sections on:        |    |
|   |  www.gitlab.com/programando-libreros/herramientas/hackpublishing    |    |
|   |                                                                     |    |
|   |         Made with Ruby | Licenses: GPLv3 (code) y LEAL (text)       |    |
|   |_____________________________________________________________________|    |
|                                                                              |
 \____________________________________________________________________________/
               \_______________________________________________/
                _______________________________________________
             _-'    .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.  --- `-_
          _-'.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.  .-.-.`-_
       _-'.-.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-`__`. .-.-.-.`-_
    _-'.-.-.-.-. .-----.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-----. .-.-.-.-.`-_
 _-'.-.-.-.-.-. .---.-. .-----------------------------. .-.---. .---.-.-.-.`-_
:-----------------------------------------------------------------------------:
`---._.-----------------------------------------------------------------._.---'
```
