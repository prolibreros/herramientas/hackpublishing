# 2.12. Mi tercer desafío

¡Ya sabes solucionar lecciones! En algunas tienes que crear ficheros o moverte
a través de directorios. Pero no solo eso, a veces también tienes que guardar
cierto contenido adentro de un fichero. Para comprobarlo, haz lo siguiente:

1. Crea un archivo llamado `mi-llave.md` con el contenido ==¡Ábrete, Sésamo!==
   adentro de `seccion02`.

> **ProTip.** Para (1) `$ echo ¡Ábrete, Sésamo! > mi-llave.md`.
